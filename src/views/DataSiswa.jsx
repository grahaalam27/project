import { CButton, CCard, CCardBody, CCardSubtitle, CCardTitle, CCol, CForm, CFormInput, CFormSelect, CFormTextarea, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow } from '@coreui/react'
import React, { useState } from 'react'
import { Container } from 'rsuite'
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
// import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link } from 'react-router-dom';
import CIcon from '@coreui/icons-react';
import { cilInfo } from '@coreui/icons';


function DataSiswa() {
  // const cellEditProp = {
  //   mode: 'click',
  //   blurToSave: true
  // };
  var products = [{
    id: 1,
    nama: "Andi",
    nis: 101,
    kelas: "10 IPA",
    alamat: "Bandung"
  }, {
    id: 2,
    nama: "Budi",
    nis: 102,
    kelas: "11 IPA",
    alamat: "Jakarta"
  }, {
    id: 3,
    nama: "Gani",
    nis: 103,
    kelas: "12 IPA",
    alamat: "Bogor"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  }, {
    id: 4,
    nama: "Dedi",
    nis: 104,
    kelas: "10 IPS",
    alamat: "Bekasi"
  },
  ];
  // function onAfterDeleteRow(rowKeys) {
  //   alert('The rowkey you drop: ' + rowKeys);
  // }
  // function onAfterInsertRow(row) {
  //   let newRowStr = '';

  //   for (const prop in row) {
  //     newRowStr += prop + ': ' + row[prop] + ' \n';
  //   }
  //   alert('The new row is:\n ' + newRowStr);
  // }
  // const selectRowProp = {
  //   mode: 'checkbox',
  //   clickToSelect: true
  // };
  // const options = {
  //   clearSearch: true,
  //   afterDeleteRow: onAfterDeleteRow,
  //   afterInsertRow: onAfterInsertRow,
  //   noDataText: 'This is custom text for empty data',

  // };

  // table2
  const columns = [{
    dataField: 'id',
    text: 'ID',
    align: 'center',
    headerAlign: 'center',
    sort: true

  }, {
    dataField: 'nama',
    text: 'Nama Lengkap',
    align: 'center',
    headerAlign: 'center',
    sort: true

  }, {
    dataField: 'nis',
    text: 'NIS',
    // filter: textFilter(),
    sort: true

  }, {
    dataField: 'kelas',
    text: 'Kelas',
    sort: true
  }, {
    dataField: 'alamat',
    text: 'Alamat'
  }, {
    dataField: 'id',
    text: 'Detail',
    formatter: CellFormatter,
  }];
  function CellFormatter(cell, row, rowIndex) {
    // return (<div><a href={cell + "/" + row.age}>{cell}</a></div>);
    return (<Link to={`/${rowIndex}`} ><CIcon icon={cilInfo}></CIcon></Link >);
  }
  const selectRow = {
    mode: 'checkbox',
    clickToSelect: false,
    selectColumnPosition: 'right'
  };

  const [add, setAdd] = useState(false)
  const [remove, setRemove] = useState(false)
  return (
    <>
      <Container className='mt-3'>
        <CRow>
          {/* JUDUL */}
          <CCol md={12}>
            <CCard>
              <CCardBody>
                <CCardTitle>SISWA</CCardTitle>
                <CCardSubtitle>data</CCardSubtitle>
              </CCardBody>
            </CCard>
          </CCol>

          {/* TABLE */}
          <CCol md={12}>
            <CCard>
              <CCardBody>
                {/* <BootstrapTable data={products} search={true} multiColumnSearch={true} options={options} insertRow selectRow={selectRowProp}
                  deleteRow multiColumnSort={2} pagination striped hover condensed >
                  <TableHeaderColumn dataField='id' isKey searchable={false} dataSort dataAlign='center'>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='nama' dataSort dataAlign='center' dataFormat={CellFormatter} filter={{ type: 'TextFilter', delay: 1000 }}>Nama</TableHeaderColumn>
                  <TableHeaderColumn dataField='nis' dataSort dataAlign='center'>NIS</TableHeaderColumn>
                  <TableHeaderColumn dataField='kelas' dataSort dataAlign='center'>Kelas</TableHeaderColumn>
                  <TableHeaderColumn dataField='alamat' dataSort dataAlign='center'>Alamat</TableHeaderColumn>

                </BootstrapTable> */}
                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-2">
                  <CButton color="success" onClick={() => setAdd(!add)}>Baru</CButton>
                  <CButton color="danger" onClick={() => setRemove(!remove)}>Hapus</CButton>
                  <CModal scrollable alignment="center" visible={remove} onClose={() => setRemove(false)}>

                    <CModalHeader>
                      <CModalTitle>Hapus Siswa</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                      Yakin Hapus Siswa?
                    </CModalBody>
                    <CModalFooter>
                      <CButton color="secondary" onClick={() => setRemove(false)}>
                        Close
                      </CButton>
                      <CButton color="primary">Hapus</CButton>
                    </CModalFooter>
                  </CModal>
                  <CModal scrollable alignment="center" visible={add} onClose={() => setAdd(false)}>
                    <CModalHeader>
                      <CModalTitle>Tambah Siswa</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                      <CForm className="row g-2">
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Nama Depan" />
                        </CCol>
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Nama Belakang" />
                        </CCol>
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="NIS" />
                        </CCol>
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="NISN" />
                        </CCol>
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Tempat" />
                        </CCol>
                        <CCol xs={6}>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Tanggal Lahir" />
                        </CCol>
                        <CCol xs={4}>
                          <CFormSelect aria-label="Default select example" label="Jenis Kelamin">

                            <option value="1">Laki-laki</option>
                            <option value="2">Perempuan</option>
                          </CFormSelect>
                        </CCol>
                        <CCol xs={4}>
                          <CFormSelect aria-label="Default select example" label="Agama">

                            <option value="1">Islam</option>
                            <option value="2">Kristen</option>
                            <option value="2">Katolik</option>
                            <option value="2">Hindu</option>
                            <option value="2">Buddha</option>
                            <option value="2">Konghuchu</option>
                          </CFormSelect>
                        </CCol>
                        <CCol xs={2}>
                          <CFormSelect aria-label="Default select example" label="Kelas">

                            <option value="1">10</option>
                            <option value="2">11</option>
                            <option value="2">12</option>
                          </CFormSelect>
                        </CCol>
                        <CCol xs={2}>
                          <CFormSelect aria-label="Default select example" label="Jurusan">

                            <option value="1">IPA</option>
                            <option value="2">IPS</option>
                          </CFormSelect>
                        </CCol>
                        <CFormTextarea
                          id="exampleFormControlTextarea1"
                          label="Alamat"
                          rows="3"
                        ></CFormTextarea>
                      </CForm>
                    </CModalBody>
                    <CModalFooter>
                      <CButton color="secondary" onClick={() => setAdd(false)}>
                        Close
                      </CButton>
                      <CButton color="primary">Tambah</CButton>
                    </CModalFooter>
                  </CModal>
                </div>
                <BootstrapTable keyField='id' columns={columns} data={products} striped hover condensed noDataIndication="Table is Empty" filter={filterFactory()} selectRow={selectRow} pagination={paginationFactory()} />
              </CCardBody>
            </CCard>
          </CCol>

        </CRow>
      </Container>
    </>
  )
}

export default DataSiswa