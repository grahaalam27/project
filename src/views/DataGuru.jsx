import { CButton, CCard, CCardBody, CCardSubtitle, CCardTitle, CCol, CContainer, CForm, CFormInput, CFormSelect, CFormTextarea, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow } from '@coreui/react'
import React, { useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import CIcon from '@coreui/icons-react';
import { cilInfo } from '@coreui/icons';


function DataGuru() {
  const columns = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
      field: 'firstName',
      headerName: 'First name',
      width: 150,
      editable: true,
    },
    {
      field: 'lastName',
      headerName: 'Last name',
      width: 150,
      editable: true,
    },
    {
      field: 'age',
      headerName: 'Age',
      type: 'number',
      width: 110,
      editable: true,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params) =>
        `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    },
    {
      field: 'detail',
      headerName: 'Detail',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      // valueFormatter: (params) => {
      //   return (<Link to={`/${params.row.id}`} ><CIcon icon={cilInfo}></CIcon></Link >)
      // },
      renderCell: (params) => (<Link to={`/${params.row.id}`} ><CIcon icon={cilInfo}></CIcon></Link >)
    },
  ];

  const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  ];
  const [add, setAdd] = useState(false)
  const [remove, setRemove] = useState(false)
  return (
    <>
      <CContainer className='mt-3'>
        <CRow>
          <CCol md={12}>
            <CCard>
              <CCardBody>
                <CCardTitle>Guru</CCardTitle>
                <CCardSubtitle>data</CCardSubtitle>
              </CCardBody>
            </CCard>
          </CCol>
          <CCol md={12}>
            <CCard>
              <CCardBody>
                <div className="d-grid gap-2 d-md-flex justify-content-md-end mb-2">
                  <CButton color="success" onClick={() => setAdd(!add)}>Baru</CButton>
                  <CButton color="danger" onClick={() => setRemove(!remove)}>Hapus</CButton>

                </div>
                <div style={{ height: 400, width: '100%' }}>
                  <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection
                    disableSelectionOnClick
                    showCellRightBorder={true}
                    showColumnRightBorder={true}
                  // rowSpacingType='border'
                  // components={components}
                  />
                </div>
                <CModal scrollable alignment="center" visible={remove} onClose={() => setRemove(false)}>

                  <CModalHeader>
                    <CModalTitle>Hapus Siswa</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    Yakin Hapus Siswa?
                  </CModalBody>
                  <CModalFooter>
                    <CButton color="secondary" onClick={() => setRemove(false)}>
                      Close
                    </CButton>
                    <CButton color="primary">Hapus</CButton>
                  </CModalFooter>
                </CModal>
                <CModal scrollable alignment="center" visible={add} onClose={() => setAdd(false)}>
                  <CModalHeader>
                    <CModalTitle>Tambah Siswa</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <CForm className="row g-2">
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Nama Depan" />
                      </CCol>
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Nama Belakang" />
                      </CCol>
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="NIS" />
                      </CCol>
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="NISN" />
                      </CCol>
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Tempat" />
                      </CCol>
                      <CCol xs={6}>
                        <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" label="Tanggal Lahir" />
                      </CCol>
                      <CCol xs={4}>
                        <CFormSelect aria-label="Default select example" label="Jenis Kelamin">

                          <option value="1">Laki-laki</option>
                          <option value="2">Perempuan</option>
                        </CFormSelect>
                      </CCol>
                      <CCol xs={4}>
                        <CFormSelect aria-label="Default select example" label="Agama">

                          <option value="1">Islam</option>
                          <option value="2">Kristen</option>
                          <option value="2">Katolik</option>
                          <option value="2">Hindu</option>
                          <option value="2">Buddha</option>
                          <option value="2">Konghuchu</option>
                        </CFormSelect>
                      </CCol>
                      <CCol xs={2}>
                        <CFormSelect aria-label="Default select example" label="Kelas">

                          <option value="1">10</option>
                          <option value="2">11</option>
                          <option value="2">12</option>
                        </CFormSelect>
                      </CCol>
                      <CCol xs={2}>
                        <CFormSelect aria-label="Default select example" label="Jurusan">

                          <option value="1">IPA</option>
                          <option value="2">IPS</option>
                        </CFormSelect>
                      </CCol>
                      <CFormTextarea
                        id="exampleFormControlTextarea1"
                        label="Alamat"
                        rows="3"
                      ></CFormTextarea>
                    </CForm>
                  </CModalBody>
                  <CModalFooter>
                    <CButton color="secondary" onClick={() => setAdd(false)}>
                      Close
                    </CButton>
                    <CButton color="primary">Tambah</CButton>
                  </CModalFooter>
                </CModal>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </>
  )
}

export default DataGuru