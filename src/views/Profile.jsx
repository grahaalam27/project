import { CButton, CCard, CCardBody, CCardFooter, CCardImage, CCardTitle, CCol, CContainer, CForm, CFormInput, CFormLabel, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow, CTable, CTableBody, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from '@coreui/react'
import React, { useState } from 'react'


function Profile() {
  const [editProfile, setEditProfile] = useState(false)
  const [editPassword, setEditPassword] = useState(false)
  return (
    <>
      <CContainer className='mt-3 mb-2'>
        <CRow>
          <CCol xs={12}>
            <CCard>
              <CCardBody>
                <CCardTitle>Profile</CCardTitle>
              </CCardBody>
            </CCard>
          </CCol>
          {/* table profile */}
          <CCol className='mt-3'>

            <CCard>
              <CRow>
                <CCol md={4} className="align-self-center m-auto" style={{ maxWidth: "150px" }}>
                  <CCardImage src='./logo.png' />
                  {/* <CImage src='./logo.png' fluid thumbnail width={200} /> */}
                </CCol>
                <CCol md={8}>

                  <CCardBody>

                    <CTable borderless>
                      <CTableHead>
                        <CTableRow>

                          <CTableHeaderCell scope="col">BIODATA</CTableHeaderCell>
                          {/* <CTableHeaderCell scope="col"></CTableHeaderCell> */}
                          <CTableHeaderCell scope="col"></CTableHeaderCell>
                        </CTableRow>
                      </CTableHead>
                      <CTableBody>
                        <CTableRow>
                          <CTableHeaderCell scope="row">Nama</CTableHeaderCell>
                          <CTableDataCell>:</CTableDataCell>
                          <CTableDataCell><CFormInput className='pt-0 pb-0' type="text" defaultValue="Admin" readOnly plainText /></CTableDataCell>
                        </CTableRow>
                        <CTableRow>
                          <CTableHeaderCell scope="row">Email</CTableHeaderCell>
                          <CTableDataCell>:</CTableDataCell>
                          <CTableDataCell><CFormInput className='pt-0 pb-0' type="email" id="staticEmail" defaultValue="email@example.com" readOnly plainText /></CTableDataCell>
                        </CTableRow>
                        <CTableRow>
                          <CTableHeaderCell scope="row">Password</CTableHeaderCell>
                          <CTableDataCell>:</CTableDataCell>
                          <CTableDataCell><CFormInput className='pt-0 pb-0' type="password" id="staticEmail" defaultValue="email@example.com" readOnly plainText /></CTableDataCell>
                        </CTableRow>
                      </CTableBody>
                    </CTable>
                  </CCardBody>
                  <CCardFooter className="bg-white border-0">
                    <CButton className='me-2' color="primary" onClick={() => setEditProfile(!editProfile)}>Edit Profile</CButton>
                    <CButton color="primary" onClick={() => setEditPassword(!editPassword)}>Edit Password</CButton>

                    <CModal scrollable alignment="center" visible={editProfile} onClose={() => setEditProfile(false)}>
                      <CModalHeader>
                        <CModalTitle>EDIT PROFILE</CModalTitle>
                      </CModalHeader>
                      <CModalBody>
                        <CForm>
                          <CFormLabel htmlFor="exampleFormControlInput1">Nama</CFormLabel>
                          <CFormInput type="text" id="exampleFormControlInput1" placeholder="Admin" aria-describedby="exampleFormControlInputHelpInline" />
                          <CFormLabel htmlFor="exampleFormControlInput1">Email</CFormLabel>
                          <CFormInput type="email" id="exampleFormControlInput1" placeholder="name@example.com" aria-describedby="exampleFormControlInputHelpInline" />
                        </CForm>
                      </CModalBody>
                      <CModalFooter>
                        <CButton color="secondary" onClick={() => setEditProfile(false)}>
                          Close
                        </CButton>
                        <CButton color="primary">Simpan</CButton>
                      </CModalFooter>
                    </CModal>

                    <CModal scrollable alignment="center" visible={editPassword} onClose={() => setEditPassword(false)}>
                      <CModalHeader>
                        <CModalTitle>EDIT PASSWORD</CModalTitle>
                      </CModalHeader>
                      <CModalBody>
                        <CForm>
                          <CFormLabel htmlFor="exampleFormControlInput1">Password Lama</CFormLabel>
                          <CFormInput type="password" id="exampleFormControlInput1" defaultValue="password" aria-describedby="exampleFormControlInputHelpInline" />
                          <CFormLabel htmlFor="exampleFormControlInput1">Password Baru</CFormLabel>
                          <CFormInput type="password" id="exampleFormControlInput1" defaultValue="password" aria-describedby="exampleFormControlInputHelpInline" />
                          <CFormLabel htmlFor="exampleFormControlInput1">Ulangi Password Baru</CFormLabel>
                          <CFormInput type="password" id="exampleFormControlInput1" defaultValue="password" aria-describedby="exampleFormControlInputHelpInline" />
                        </CForm>
                      </CModalBody>
                      <CModalFooter>
                        <CButton color="secondary" onClick={() => setEditPassword(false)}>
                          Close
                        </CButton>
                        <CButton color="primary">Simpan</CButton>
                      </CModalFooter>
                    </CModal>

                  </CCardFooter>
                </CCol>
              </CRow>
            </CCard>


          </CCol>
          {/* image */}

        </CRow>
      </CContainer>
    </>
  )
}

export default Profile