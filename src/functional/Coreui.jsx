import { CCollapse, CContainer, CNavbar, CNavbarBrand, CNavbarText, CNavbarToggler } from '@coreui/react'
import React, {  useState } from 'react'
import '@coreui/coreui/dist/css/coreui.min.css'

function Coreui() {
  const [visible, setVisible] = useState(false)
  return (
    <>
      <CNavbar colorScheme="light" className="bg-light">
        <CContainer fluid>
          <CNavbarBrand className="mb-0 h1">SMA AL-Falah Dago</CNavbarBrand>
          <CNavbarToggler aria-controls='sidebar' onClick={() => setVisible(!visible)} />
          <CNavbarText>Kelas : XII IPA</CNavbarText>
        </CContainer>
      </CNavbar>

      <CCollapse id="navbarToggleExternalContent" visible={visible}>
        <div className="bg-dark p-4">
          <h5 className="text-white h4">Collapsed content</h5>
          <span className="text-medium-emphasis-inverse">Toggleable via the navbar brand.</span>
        </div>
      </CCollapse>
      <CNavbar colorScheme="dark" className="bg-dark">
        <CContainer fluid>
          <CNavbarToggler
            aria-controls="navbarToggleExternalContent"
            aria-label="Toggle navigation"
            onClick={() => setVisible(!visible)}
          />
        </CContainer>
      </CNavbar>
    </>
  )
}

export default Coreui