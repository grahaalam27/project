import CIcon from '@coreui/icons-react'
import { CAvatar, CContainer, CDropdown, CDropdownDivider, CDropdownItem, CDropdownItemPlain, CDropdownMenu, CDropdownToggle, CFooter, CHeader, CHeaderNav, CHeaderText, CHeaderToggler, CNavGroup, CNavItem, CSidebar, CSidebarBrand, CSidebarNav } from '@coreui/react'
import React, { useState } from 'react'
import { cilMenu } from '@coreui/icons';
import { Link, Route, Routes } from 'react-router-dom';

import Dashboard from '../views/dashboard/Dashboard';
import Mapel from '../views/masterdata/Mapel';
import Jurusan from '../views/masterdata/Jurusan';
import Kelas from '../views/masterdata/Kelas';
import TahunAkademik from '../views/masterdata/TahunAkademik';
import DataSiswa from '../views/DataSiswa';
import DataGuru from '../views/DataGuru';
import Profile from '../views/Profile';

function Test() {
  // const dispatch = useDispatch()
  // const unfoldable = useSelector((state) => state.sidebarUnfoldable)
  // const sidebarShow = useSelector((state) => state.sidebarShow)
  const [visible, setVisible] = useState(true)
  return (
    <>
      <div className="d-flex">
        <CSidebar visible={visible} className="bg-gray">
          <CSidebarBrand><CAvatar src="./logo.png" size="sx" className='me-2' />SMA AL-Falah Dago</CSidebarBrand>
          <CSidebarNav>
            <Link to="/" className='text-light text-decoration-none'>
              <CNavItem href="#">
                Dashboard
              </CNavItem>
            </Link>

            <CNavGroup toggler="Master Data" className='text-light'>
              <Link to="/kelas" className='text-light text-decoration-none'>
                <CNavItem href="#">
                  Kelas
                </CNavItem>
              </Link>

              <Link to="/tahun-akademik" className='text-light text-decoration-none'>
                <CNavItem href="#">
                  Tahun Akademik

                </CNavItem>
              </Link>
              <Link to="/jurusan" className='text-light text-decoration-none'>
                <CNavItem href='#'>
                  Jurusan
                </CNavItem>
              </Link>
            </CNavGroup>
            <Link to="/data-guru" className='text-light text-decoration-none'>
              <CNavItem href='#'>
                Data Guru
              </CNavItem>
            </Link>
            <Link to="/data-siswa" className='text-light text-decoration-none'>
              <CNavItem href='#'>
                Data Siswa
              </CNavItem>
            </Link>
            <Link to="/profile" className='text-light text-decoration-none'>
              <CNavItem href='#'>
                Profile
              </CNavItem>
            </Link>
          </CSidebarNav>
        </CSidebar>

        <div className="wrapper d-flex flex-column min-vh-100 w-100 bg-light">

          <CHeader position='sticky' >
            <CContainer fluid>
              <CHeaderToggler
                onClick={() => setVisible(!visible)}
              >
                <CIcon icon={cilMenu}></CIcon>
              </CHeaderToggler>
              <CHeaderText className='ms-auto me-3' >Kelas : -</CHeaderText>
              <CHeaderNav>
                <CDropdown variant='nav-item'>
                  <CDropdownToggle placement="bottom-end" className='py-0 text-decoration-none' caret={false}>
                    Admin
                  </CDropdownToggle>
                  <CDropdownMenu className='pt-0' placement="bottom-end">
                    <CDropdownItem disabled className='text-center'>
                      <CAvatar src="./logo.png" size="md" />
                    </CDropdownItem>
                    <CDropdownItem disabled className='text-center'>
                      Super Admin
                    </CDropdownItem>
                    <CDropdownDivider />
                    <CDropdownItemPlain className='d-flex flex-row'>
                      <Link to="/profile" className='text-light text-decoration-none'>
                        <CDropdownItem href='#' className='text-decoration-none'>
                          Profile
                        </CDropdownItem>
                      </Link>
                      <CDropdownItem href='#' className='text-decoration-none'>
                        Log Out
                      </CDropdownItem>
                    </CDropdownItemPlain>
                  </CDropdownMenu>
                </CDropdown>
              </CHeaderNav>
            </CContainer>
          </CHeader>

          <div className="body flex-grow-1 px-3">
            <Routes>
              {/* sidebar */}
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/mata-pelajaran" element={<Mapel />} />
              <Route path="/jurusan" element={<Jurusan />} />
              <Route path="/kelas" element={<Kelas />} />
              <Route path="/tahun-akademik" element={<TahunAkademik />} />
              <Route path="/data-guru" element={<DataGuru />} />
              <Route path="/data-siswa" element={<DataSiswa />} />
              <Route path="/profile" element={<Profile />} />

              {/* detail */}

            </Routes>

            <CFooter className='position-relative bottom-0 '>
              <span> Copyright &copy; 2022 SMA-AL-Falah Dago</span>
            </CFooter>
          </div>
        </div>
      </div>

    </>
  )
}

export default Test